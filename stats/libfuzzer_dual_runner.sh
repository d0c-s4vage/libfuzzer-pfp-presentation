DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
THIS_PROG="$0"

cd "$DIR"/../

MAX_COUNT="$1"
HALF_COUNT=$(( $MAX_COUNT / 2 ))

for iter in {0..9} ; do 
    corpus=/tmp/libfuzzer_dual_corpus_$MAX_COUNT
    output=/tmp/libfuzzer_dual_stats_$MAX_COUNT.txt
    mkdir -p "$corpus" ; rm "$corpus"/*

    echo "iter: $iter"

    MAX_COUNT="$HALF_COUNT" \
    LIBFUZZER_PYTHON_MODULE=pfp_fuzzer \
        PFP_TEMPLATE=templates/PNG_full.bt \
        PYTHONPATH=examples \
        ASAN_OPTIONS=detect_leaks=0 \
        ./fuzzMe_libpng \
            "$corpus" \
            seeds \
            -artifact_prefix=crashes/ >/dev/null 2>&1

    ASAN_OPTIONS=detect_leaks=0 \
        ./fuzzMe_libpng  \
            "$corpus" \
            seeds \
            -artifact_prefix=crashes/ \
            -runs=$HALF_COUNT \
        2>&1 | grep -v libpng > "$output"

    grep ' cov: ' "$output" | tail -n 1 | awk '{print $4 "," $6, "," $10}' >> "$DIR/libfuzzer_stats_dual_${MAX_COUNT}.csv"
done
