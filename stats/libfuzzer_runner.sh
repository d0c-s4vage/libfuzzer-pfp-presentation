#!/usr/bin/env bash


DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
THIS_PROG="$0"

cd "$DIR"/../

MAX_COUNT="$1"

for iter in {0..9} ; do 
    corpus=/tmp/libfuzzer_corpus_$MAX_COUNT
    output=/tmp/libfuzzer_stats_$MAX_COUNT.txt
    mkdir -p "$corpus" ; rm "$corpus"/*

    echo "iter: $iter"

    ASAN_OPTIONS=detect_leaks=0 \
        ./fuzzMe_libpng  \
            "$corpus" \
            seeds \
            -artifact_prefix=crashes/ \
            -runs=$MAX_COUNT \
        2>&1 | grep -v libpng > "$output"

    grep ' cov: ' "$output" | tail -n 1 | awk '{print $4 "," $6, "," $10}' >> "$DIR/libfuzzer_stats_${MAX_COUNT}.csv"
done
