#!/usr/bin/env python3

from collections import defaultdict
import glob
import os
import re

THIS_DIR = os.path.dirname(__file__)

categories = defaultdict(dict)

min_val = 90
max_val = 150
bar_width = 40

full = "█"
half = "▌"


def bar(value, min_val, max_val, width):
    if not (min_val <= value <= max_val):
        raise ValueError("Value {} out of bar range".format(value))
    total_range = max_val - min_val
    num_halves = width * 2
    value_halves = int(round((num_halves / float(total_range)) * (value - min_val)))
    num_full_blocks = value_halves // 2
    res = (full * num_full_blocks) + (half * (num_full_blocks % 2)) + " {:0.1f}".format(value)
    return res


def update_range(range_holder, val):
    if val < range_holder[0]:
        range_holder[0] = int(val)
    if val > range_holder[1]:
        range_holder[1] = int(val)


def stat_type(filename):
    if "dual" in filename:
        return "dual"
    if "pfp" in filename:
        return "pfp"
    return "libfuzzer"


cov_range = [10000, 0]
ft_range = [10000, 0]
for filepath in glob.glob(os.path.join(THIS_DIR, "*.csv")):
    number = re.match(r'.*_(\d*).csv', filepath).group(1)
    number = int(number)

    rows = total_cov = total_ft = total_execs = 0
    with open(filepath, "r") as f:
        for line in f:
            if line.strip() == "":
                continue
            cov, ft, execs = [int(x.strip()) for x in line.split(",")]
            rows += 1
            total_cov += cov
            total_ft += ft
            total_execs += execs

    avg_cov = total_cov / float(rows)
    update_range(cov_range, avg_cov)

    avg_ft = total_ft / float(rows)
    update_range(ft_range, avg_ft)

    categories[number][filepath] = {
        "cov": avg_cov,
        "ft": avg_ft,
    }


for num_iters in sorted(categories.keys()):
    stat_files = categories[num_iters]
    print("|  |  |  |")
    for stat_file in sorted(stat_files.keys()):
        file_info = stat_files[stat_file]
        print("| {} - {} | {} | {} |".format(
            num_iters,
            stat_type(stat_file),
            bar(file_info["cov"], cov_range[0]-10, cov_range[1]+10, bar_width), 
            bar(file_info["ft"], ft_range[0]-10, ft_range[1]+10, bar_width), 
        ))
