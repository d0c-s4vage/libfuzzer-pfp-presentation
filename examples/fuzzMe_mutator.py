import random

chars = list(range(0, 0x100))

def custom_mutator(data, max_size, seed, native_mutator):
    random.seed(seed)
    res = bytearray(data)

    if random.randint(0, 1) == 1:
        rand_idx = random.randint(0, len(data)-1)
        res[rand_idx] = random.choice(chars)

    # add a new byte
    if random.randint(0, 1) == 1:
        res += bytearray([random.choice(chars)])
    # remove a byte
    elif len(res) > 1 and random.randint(0, 1):
        res = res[:-1]

    return res
