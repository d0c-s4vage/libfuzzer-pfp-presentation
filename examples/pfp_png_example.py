import pfp

dom = pfp.parse(data_file="examples/example.png", template_file="templates/PNG.bt")
text_chunk = dom.chunks[3]

def save_png(dom, path):
    with open(path, "wb") as f:
        f.write(dom._pfp__build())

def use_metadata_template():
    dom = pfp.parse(data_file="examples/example.png", template_file="templates/PNG_metadata.bt")
    text_chunk = new_dom.chunks[3]

def exiftool(path):
    import os
    os.system("exiftool {}".format(path))
    
print(text_chunk._pfp__show())

use_metadata_template()
