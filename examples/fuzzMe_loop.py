import random

def custom_loop(do_run_one, files, next_data=None):
    if next_data is None:
        next_data = bytearray()
    next_data.extend([0])
    for x in range(0, 0x100):
        next_data[-1] = x
        if not do_run_one(next_data):
            continue
        custom_loop(do_run_one, None, next_data)
    next_data.pop()
