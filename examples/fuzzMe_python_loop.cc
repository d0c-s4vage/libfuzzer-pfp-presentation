#include <stdint.h>
#include <stddef.h>

bool FuzzMe(const uint8_t *Data, size_t DataSize) {
  return DataSize >= 1 && Data[0] == 'F' &&
      DataSize >= 2 && Data[1] == 'U' &&
      DataSize >= 3 && Data[2] == 'Z' &&
      Data[3] == 'Z';  // :‑<
}

extern "C" int LLVMFuzzerTestOneInput(const uint8_t *Data, size_t Size) {
  FuzzMe(Data, Size);
  return 0;
}

#include "../bridge/python_bridge_loop.cpp"
