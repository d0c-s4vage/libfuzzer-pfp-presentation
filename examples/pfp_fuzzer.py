#!/usr/bin/env python
# encoding: utf-8


import glob
from io import BytesIO
import os
import pfp
import pfp.fields as fields
import pfp.fuzz
import pfp.fuzz.rand
import pfp.utils as utils
import re
import random
import six
import sys
import traceback


PFP_INTERP = pfp.create_interp(template_file=os.environ["PFP_TEMPLATE"])


class FuzzerBasic(pfp.fuzz.StratGroup):
    """A basic strategy that has FieldStrats (field strategies) defined
    for every field type. Nothing fancy, just basic.
    """

    name = "fuzzer_basic"

    class Ints(pfp.fuzz.FieldStrat):
        klass = [fields.IntBase]#fields.Short, fields.Int, fields.Int64]

        def prob(self, field):
            # generate the probabilities table
            offset = 0

            max_val = 1 << (field.width * 8)

            if field.signed:
                min_val = int(-max_val / 2)
                max_val = int(max_val / 2) - 1
                vals = [min_val, max_val, 0]
                vals += [1 << (x * 8) for x in six.moves.range(field.width)]
                vals += [-x for x in vals]
                vals = sorted(set(vals))
                vals = vals[:-1]

            else:
                min_val = 0
                max_val -= 1
                vals = [0]
                vals += [
                    1 << (x * 4) for x in six.moves.range(field.width * 2)
                ]
                vals.append((1 << (field.width * 8)) - 1)

            # now add the +-1 values
            new_vals = []
            for val in vals[2:-1]:
                new_vals.append(val - 1)
                new_vals.append(val + 1)
            vals += new_vals

            increment_vals = []
            num_on_sides = 4
            for x in range(num_on_sides):
                x += 1
                new_val = field._pfp__value + x
                if new_val < max_val:
                    increment_vals.append(new_val)
                new_val = field._pfp__value - x
                if new_val > min_val:
                    increment_vals.append(new_val)

            pv = 1.0 / 4.0
            res = [
                (pv, increment_vals),
                (pv, vals),
                (pv, six.moves.xrange(0, min(max_val, 0x100))),
                (pv, {"min": min_val, "max": max_val}),
            ]

            return res

    class Float(pfp.fuzz.FieldStrat):
        klass = fields.Float
        prob = [(1.0, lambda: pfp.fuzz.rand.randfloat(-0x10000000, 0x10000000))]

    class Double(pfp.fuzz.FieldStrat):
        klass = fields.Double
        prob = [(1.0, lambda: pfp.fuzz.rand.randfloat(-0x10000000, 0x10000000))]

    class Enum(Ints):
        klass = fields.Enum

        def prob(self, field):
            # treat it the same as ints, with the addition of the actual (valid)
            # enum values
            res = super(FuzzerBasic.Enum, self).prob(field)

            # add in the new enum values
            prob_percent = 1.0 / float(len(res) + 5)
            res = [(prob_percent, x[1]) for x in res]
            res.append((prob_percent * 5, list(filter(
                lambda x: not isinstance(x, six.string_types),
                field.enum_vals.keys(),
            ))))

            return res

    class String(pfp.fuzz.FieldStrat):
        klass = fields.String
        all_chars = [utils.binary(chr(x)) for x in six.moves.range(0x100)]

        def next_val(self, field):
            rand_data_size = len(field)
            res = pfp.fuzz.rand.data(rand_data_size, self.all_chars)

            if pfp.fuzz.rand.maybe():
                res += res[:-1] + utils.binary("\x00")

            return res

    class Array(pfp.fuzz.FieldStrat):
        klass = fields.Array
        all_chars = [utils.binary(chr(x)) for x in six.moves.range(0x100)]

        def next_val(self, field):
            rand_data_size = len(field)
            res = pfp.fuzz.rand.data(rand_data_size, self.all_chars)
            return res

    FILTER_NAME_REGEX = re.compile(r'(crc|ctype|cname)', re.IGNORECASE)

    def filter_fields(self, fields):
        return filter(
            lambda x: not self.FILTER_NAME_REGEX.search(x._pfp__name.lower()),
            fields,
        )


def print_flush(msg):
    print(msg)
    sys.stdout.flush()


COUNT = 0
def custom_loop(do_run_one, files):
    print_flush("CUSTOM LOOP")
    print_flush("Will fuzz {} files".format(len(files)))

    def counted_run_one(data):
        global COUNT
        COUNT += 1
        if COUNT == int(os.environ.get("MAX_COUNT", -1)):
            sys.stdout.flush()
            raise Exception("DONE")
        return do_run_one(data)

    pfp.fuzz.rand.RANDOM.shuffle(files)
    while True:
        for filename in files:
            fuzz_filepath(filename, counted_run_one)


def fuzz_filepath(filepath, do_run_one, dom=None, depth=1):
    if depth == 100:
        return

    if dom is None:
        with open(filepath, "rb") as f:
            dom = PFP_INTERP.parse(f, keep_successful=True)

    opts = dict(
        num=max(int(10000 / float(depth)), 1000),
        yield_changed=True,
    )

    for num_at_once in range(1, 4):
        opts["at_once"] = num_at_once

        for mutated, changed_fields in pfp.fuzz.changeset_mutate(dom, FuzzerBasic, **opts):
            new_cov = do_run_one(mutated)
            if new_cov:
                print_flush("  | {} Changed {}".format(
                    ">" * depth,
                    ", ".join("{}:{}".format(x, x._pfp__path()) for x in changed_fields)),
                )
                for mutated in fuzz_fields(dom, FuzzerBasic, changed_fields, num=100, at_once=len(changed_fields), fields_to_modify=changed_fields):
                    if do_run_one(mutated):
                        fuzz_filepath(filepath, do_run_one, dom, depth+1)
                fuzz_filepath(filepath, do_run_one, dom, depth+1)


def fuzz_fields(dom, strat, do_run_one, **opts):
    for mutated in pfp.fuzz.changeset_mutate(dom, strat, **opts):
        yield mutated


if __name__ == '__main__':
    custom_loop(lambda x: pfp.fuzz.rand.maybe(0.5), ["seeds/seed.png"])
