---
author: James Johnson - @d0c_s4vage
title: Coverage-based Fuzzing with LibFuzzer and pfp
date: 2020-02-26
extensions:
- qrcode
styles:
    table:
        column_spacing: 5
---

# Who Am I

James Johnson

```qrcode-ex
columns:
  - data: https://twitter.com/d0c_s4vage
    caption: 'Twitter: @d0c_s4vage'
```

# Who Am I: II

* Security Research


* Tool/Software Development


* Bouldering is my latest hobby

Currently @ GitLab on the Vulnerability Research team

# LibFuzzer & PFP

This presentation will cover some research I've done into

* Integrating format-aware tools I've written to work with libfuzzer


* Pros and cons of speed vs "awareness"


* Possible positives that come out of using a blended approach

# Context: Fuzzing Basics

## Fuzzing

* Generate inputs


* Feed inputs to an instrumented/debugged program


* Capture crashes/error conditions

# Context: Fuzzing Basics

## Dumb Fuzzing

* Randomly generate/modify input data

## Grammar-Based Fuzzing

* Use a grammar to define acceptable inputs

## Format-Aware Fuzzing

* Have a seed corpus of data that is parsed and intelligently modified

# Fuzzer Feedback

## Question: How Do You Know If You're Making Progress?

Input A has run through your test harness.

How do we know if it helped us make progress towards finding bugs?

* Haven't tested that input before


* Found a bug


* Code coverage


* ???

# Fuzzer Feedback: II

Whatever your metric is to determine if you're making progress, that metric
is the feedback you can use to help guide your fuzzer.

If you're making "progress", what do you do?


* Continue sending similar test cases


* Fuzz additionally on top of the case that made progress

# Coverage-Based Fuzzers

Below are a few examples of coverage-based fuzzers

|    Fuzzer | Homepage                                    |
|----------:|---------------------------------------------|
| LibFuzzer | https://llvm.org/docs/LibFuzzer.html        |
|       AFL | https://github.com/google/AFL               |
|    WinAFL | https://github.com/googleprojectzero/winafl |
| HonggFuzz | https://github.com/google/honggfuzz         |
|   Go-Fuzz | https://github.com/dvyukov/go-fuzz          |

Many variations based on these fuzzers also exist. E.g.
[afl-unicorn](https://github.com/Battelle/afl-unicorn)

# Coverage-Based Fuzzers

Programs being fuzzed by coverage-based fuzzers need to be instrumented.

This can be done in two ways:

* Compiled in (LibFuzzer, AFL, HonggFuzz, Go-Fuzz)


* Dynamically inserted into binary (WinAFL)

# LibFuzzer: Basics

* Is part of the LLVM project


* Uses compile-time instrumentation to track code-coverage


# LibFuzzer: Usage

Create a `C/C++` file that:

* implements one of the external LibFuzzer functions


* is compiled with clang++, with sanitizers (ASAN/MSAN)

# LibFuzzer: External Functions

LibFuzzer is designed to work with a number of externally-defined functions.

This list of functions can be found in the `FuzzerExtFunctions.def` and
`FuzzerMain.cpp` file.

The target `C/C++` file may implement one of the following:

|             External Function | Note                                            |
|------------------------------:|-------------------------------------------------|
|    `LLVMFuzzerTestOneInput()` | Main *"test harness"* function                  |
|      `LLVMFuzzerInitialize()` | Performs any initialization that needs to occur |
|   `LLVMFuzzerCustomMutator()` | An optional external custom mutator             |
| `LLVMFuzzerCustomCrossOver()` | An optional function to blend two test cases    |

# LibFuzzer: Basic Flow

Below is a simplified version of the execution flow of libFuzzer:

```c
    FuzzerDriver()
      ┣━━ LLVMFuzzerInitialize()
      ┣━━ ReadCorpora()
      ┗━━ Fuzzer::Loop()
          ┣━━ ReadAndExecuteSeedCorpora() // Determine cov with current corpus
          ┗━━ while (true)
              ┣━━ RereadOutputCorpus() if needed
              ┗━━ MutateAndTestOne()
            ▲     ┣━━ newData = Mutate(CorpusFile) // LLVMFuzzerCustomMutator
            ┃     ┃                                // LLVMFuzzerCustomCrossover
            ┃     ┣━━ hasNewCov = LLVMFuzzerTestOneInput(newData)
            ┃     ┣━━ ReportNewCoverage(newData) if hasNewCov
            ┗━━━━━┛
```

# LibFuzzer: Example Program

Below is a single function that we'll fuzz using libFuzzer (borrowed from Google's
fuzzing tutorial [link](https://github.com/google/fuzzing/blob/master/tutorial/libFuzzer/fuzz_me.cc)):

```file
path: ./examples/fuzzMe.cc
lang: c
relative: true
lines:
  end: 10
```

# LibFuzzer: Example Program

Our `LLVMFuzzerTestOneInput` function must call the `FuzzMe` function with
the provided data and size:

```file
path: ./examples/fuzzMe.cc
lang: c
relative: true
lines:
  start: 10
```

# LibFuzzer: Example Compilation

For this example, both the code to fuzz and the libFuzzer-specific code are
defined in the same file: `examples/fuzzMe.cc`

```terminal-ex
command: bash -il
rows: 10
init_wait: lptp
init_text: clang++ -fsanitize=address,fuzzer examples/fuzzMe.cc -o fuzzMe
```

# LibFuzzer: Example Running

Below is the compiled binary running. Notice that it found a crashing input
and has saved the contents of it to `crash-XXXXX`

```terminal-ex
command: bash -il
rows: 20
init_wait: lptp
init_text: ./fuzzMe
```

# 010 Editor

[010 Editor](https://www.sweetscape.com/010editor/) by SweetScape Software.

010 Editor:

* is a hex editor
* data parser
* runs on Mac, Linux, and Windows.

# 010 Editor: Templates

010 Editor uses [templates](https://www.sweetscape.com/010editor/repository/templates/)
that define how to parse data.

Templates are interpreted and use a modified 'C' syntax:

* Declarations == parsing
* Structs can contain control flow logic
* User-defined functions
* Parsing-specific quirks/features

# 010 Editor: Basic PNG Template

The template below is a basic PNG-parsing 010 template:

```file
path: templates/PNG.bt
relative: true
lang: c
```

# 010 Editor: Demo

DEMO

# PFP: Python Format Parser

* 010 Editor Templates
* A Python-based interpreter for 010 Templates
* Was my first Python project when I worked at Endgame, rewrote it years later
* vim plugin! [pfp-vim](https://github.com/d0c-s4vage/pfp-vim)

# PFP: Composition

## pfp

https://github.com/d0c-s4vage/pfp

The bulk of the code, includes the interpreter, debugger, fuzzing code, etc.

## py010parser

https://github.com/d0c-s4vage/py010parser

py010parser lexes/parses 010 templates into an AST

# PFP: Installation

```bash
pip install pfp
```

Or, if you prefer more isolated installations, install `pipx` and install with

```bash
pipx install pfp
```

# PFP: PNG Example

```terminal-ex
command: bash -il
rows: 20
init_text: pfp -t templates/PNG.bt examples/example.png | less
init_wait: lptp
```

# PFP: PNG Interactive Example

```file
path: examples/pfp_png_example.py
relative: true
lang: python
lines:
  end: 4
```

```terminal-ex
command: bash -il
rows: 15
init_text: python3 examples/pfp_png_example.py
init_wait: lptp
init_codeblock: false
```

# PFP: Extensions to 010 Template Syntax

You may have noticed in the previous slide that the `crc` and `length` fields
did not change after the comment was changed.

PFP [adds extensions](https://pfp.readthedocs.io/en/latest/metadata.html) to
010 Editor's special attributes to support event-driven DOM modificiations.

```c
int type;
int length <watch=value, update=WatchLength>;
char value[length];
```

# PFP: Extensions to 010 Template Syntax

Below is the same PNG template as before with the extra metadata added in:

```file
path: templates/PNG_metadata.bt
relative: true
lang: c
```

# PFP: Fuzzing

Fuzzing with pfp is intended to be relatively straightforward:

* Parse data with a template
* Choose/create a strategy to use for mutations
    * The `"basic"` strategy is built-in
* Create new mutations of the `dom` using the `pfp.fuzz.mutate` function

# PFP: Fuzzing Example

```python
dom = pfp.parse(...)
for mutated_dom in pfp.fuzz.mutate(dom, **opts):
    data = mutated_dom._pfp__build()
    send_to_target(data)
```

Below are the available fields that can be used with `pfp.fuzz.mutate()`:

|           option | note                                 |
|-----------------:|--------------------------------------|
|        `at_once` | Number of fields to modify at once   |
|            `num` | Number of iterations                 |
| `yield_modified` | Return the fields that were modified |


# PFP: Fuzzing Strategies

PFP strategies are intended to help define distinct, reusable, and swappable
strategies for use during fuzzing.

A strategy may define:

* Which field types to fuzz (or ignore)
* Arbitrary choosing of fields to fuzz (e.g. ignore all fields named "data")
* How new values are chosen for specific field types

# PFP: Fuzzing Strategies: Example

The strategy below mutates all integer-based fields using only the values
`0`, `1`, `2`, and `3`, ignoring all fields with `data` in their name:

```python
class IntegersOnly(pfp.fuzz.StratGroup):
    name = "ints_only"

    class IntStrat(pfp.fuzz.FieldStrat):
        klass = pfp.fields.IntBase
        choices = [0, 1, 2, 3]

    def filter_fields(self, fields):
        return filter(lambda x: "data" not in x._pfp__name.lower(), fields)

for mutated in pfp.fuzz.mutate(dom, IntegersOnly, **opts):
    do_something(mutated)
```

# LibFuzzer & PFP: LibFuzzer Python Bridge

## About

* By: Mozilla Security Team
* https://github.com/MozillaSecurity/libfuzzer-python-bridge

## Summary

Allows Python code to be used as the `LLVMFuzzerCustomMutate` function, with
minimal changes to the main fuzzing file.

* `LIBFUZZER_PYTHON_MODULE` env variable controls which python module to load.
* `custom_mutate(data, max_size, seed, native_mutator)` must be implemented in the python module
* Fuzzing binary must be compiled with `-DCUSTOM_MUTATOR` option
* May need to set `PYTHONPATH=.`

# LibFuzzer & PFP: Python Bridge Example Target Code

Modified sample target that includes python_bridge at the end of the file.

```file
path: examples/fuzzMe_python.cc
relative: true
lang: c
```

# LibFuzzer & PFP: Python Bridge Example Python

```file
path: examples/fuzzMe_mutator.py
relative: true
lang: python
```

# LibFuzzer & PFP: Python Bridge Example Compilation

* Needs to be linked with python libraries
* Needs `-DCUSTOM_MUTATOR` to be set

```bash
clang++ \
    -DCUSTOM_MUTATOR \
    -fsanitize=address,fuzzer \
    -lpython3.7m \
    -I/usr/include/python3.7m \
    -I../../libfuzzer \
    -o fuzzMe_python
        examples/fuzzMe_python.cc \
```

```terminal-ex
command: bash -il
init_text: clang++ -DCUSTOM_MUTATOR -fsanitize=address,fuzzer examples/fuzzMe_python.cc -o fuzzMe_python -lpython3.7m -I/usr/include/python3.7m -I../../libfuzzer
init_codeblock: false
init_wait: lptp
```

# LibFuzzer & PFP: Python Bridge Example Running

Once the fuzzing binary is compiled with the python bridge, the python module
can be controlled with the `LIBFUZZER_PYTHON_MODULE` env variable:

```terminal-ex
command: bash -il
rows: 15
init_text: ASAN_OPTIONS=detect_leaks=0 LIBFUZZER_PYTHON_MODULE=fuzzMe_mutator PYTHONPATH=examples ./fuzzMe_python 2>&1 | less
init_wait: lptp
```

# Modifications: Problems

If PFP were to be used as the custom mutator, we'd have to re-parse the
provided data each time, which is rather expensive.

What if we allowed PFP to make have direct feedback from test results?

Then we could:

* Maintain state
* Optimize field mutations

However, in order for this to occur, we'd have to make a few changes.

# Modifications: LibFuzzer

In order to pull the logic out of LibFuzzer and into Python code, changes had
to be made in:

## compiler-rt/lib/fuzzer

Refactor `Fuzzer::Loop` and related files to allow an external
`LLVMFuzzerCustomLoop` to be defined

```c
extern "C"
void LLVMFuzzerCustomLoop(void(*DoCoreLoop)(void *F_),
                          bool(*DoRunOne)(void *F_, const uint8_t *Data, size_t Size),
                          void *F_,
                          fuzzer::Vector<fuzzer::SizedFile> &CorporaFiles) {
    // code
}
```

See [my libfuzzer fork](https://gitlab.com/d0c-s4vage/libfuzzer) for the
modifications.

# Modifications: LibFuzzer

In order to pull the logic out of LibFuzzer and into Python code, changes had
to be made in:

## libfuzzer-python-bridge

Implement the `LLVMFuzzerCustomLoop` function so that a python function,
`custom_loop`, could be defined with the signature:

```python
def custom_loop(do_run_one, files):
    # completely drive the fuzzing process
    for filepath in files:
        new_data = mutate_data(read_file(filepath))

        while True:
            new_coverage = do_run_one(new_data)
            if not new_coverage:
                # no new coverage, break
                break
            # has new coverage, keep mutating this data
            new_data = mutate_data(new_data)
```

See [my libfuzzer-python-bridge fork](https://github.com/d0c-s4vage/libfuzzer-python-bridge)
for the modifications

# Modifications: Compiling Modified LibFuzzer 

* The code we modified is in `compiler-rt/lib/fuzzer` in the llvm-project source
tree.


* `compiler-rt/lib/fuzzer` can be compiled independently from llvm


* The installed `libFuzzer.a` used during compilation needs to be updated with
  the new `libFuzzer.a`.
  

* For me this was at The resulting `libFuzzer.a` static library only needs to be compiled against
  to include the changes

# Modifications: FuzzMe Example - PythonLoop

Here is a simple brute-force fuzzing loop that will continue fuzzing on top
of inputs that triggered new coverage:

```file
path: examples/fuzzMe_loop.py
relative: true
lang: python
```

# Modifications: FuzzMe Example

```file
path: examples/fuzzMe_python_loop.cc
relative: true
lang: c
```

# Modifications: Compiling

Need to include the new `libFuzzer.a` and `-DCUSTOM_MUTATOR` is not needed anymore.

```bash
clang++ \
    -fsanitize=address,fuzzer \
    -lpython3.7m \
    -I/usr/include/python3.7m \
    -I../../libfuzzer \
    -o fuzzMe_python_loop \
        examples/fuzzMe_python_loop.cc \
        libFuzzer.a
```

```terminal-ex
command: bash -il
init_text: clang++ -fsanitize=address,fuzzer examples/fuzzMe_python_loop.cc -o fuzzMe_python_loop libFuzzer.a -lpython3.7m -I/usr/include/python3.7m -I../../libfuzzer
init_codeblock: false
init_wait: lptp
```

# Modifications: Running

```file
path: examples/fuzzMe_python_loop.cc
relative: true
lang: c
lines:
  start: 3
  end: 10
```

```terminal-ex
command: bash -il
rows: 10
init_text: ASAN_OPTIONS=detect_leaks=0 LIBFUZZER_PYTHON_MODULE=fuzzMe_loop_debug PYTHONPATH=examples ./fuzzMe_python_loop
init_wait: lptp
```

# PFP & libpng: Setup

Setup for libpng is similar to other examples we've done, except with more
bootstrap and setup:

```file
path: examples/libpng_fuzzer.cc
lang: c
transform: grep -A50 LLVMFuzzerTestOneInput
lines:
    end: 20
```

# PFP & libpng: Notes About Setup

## Google OSS Fuzz

Google's OSS fuzz program has *MANY* examples of libfuzzer setups.

See their GitHub repo at: https://github.com/google/oss-fuzz/tree/master/projects

# PFP & libpng: libpng

libpng itself needs to be compiled with clang with the `-fsanitize=address`

# PFP & libpng: Compilation

```bash
clang++ \
    -fsanitize=address,fuzzer \
    -lpython3.7m \
    -lz \
    -I/usr/include/python3.7m \
    -I../../libfuzzer \
    -o fuzzMe_libpng \
        examples/libpng_fuzzer.cc \
        libFuzzer.a \
        examples/libpng/.libs/libpng16.a
```

```terminal-ex
command: bash -il
init_text: clang++ -fsanitize=address,fuzzer -lpython3.7m -lz -I/usr/include/python3.7m -I../../libfuzzer -o fuzzMe_libpng examples/libpng_fuzzer.cc libFuzzer.a examples/libpng/.libs/libpng16.a
init_wait: lptp
init_codeblock: false
```

# PFP & libpng: PFP Loop

Now that we are able to get feedback directly in Python from libFuzzer,
we need the python loop that uses PFP:

```file
path: examples/pfp_fuzzer.py
lang: python
transform: grep -A9 "def custom_loop"
```

# PFP & libpng: PFP Mutation

Notes:

* dom is parsed once per file
* Uses an optimized mutation function `changeset_mutate()`
* Successful mutations are further mutated

# PFP & libpng: PFP Mutation

```file
path: examples/pfp_fuzzer.py
lang: python
transform: grep -A28 "def fuzz_filepath"
```

# PFP & libpng: Running

```bash
LIBFUZZER_PYTHON_MODULE=pfp_fuzzer \
PFP_TEMPLATE=templates/PNG_full.bt \
PYTHONPATH=examples \
ASAN_OPTIONS=detect_leaks=0 \
    ./fuzzMe_libpng fuzzed_corpus seeds -artifact_prefix=crashes/ 2>&1 | grep -v libpng
```

```terminal-ex
command: bash -il
init_text: venv3 ; LIBFUZZER_PYTHON_MODULE=pfp_fuzzer PFP_TEMPLATE=templates/PNG_full.bt PYTHONPATH=examples ASAN_OPTIONS=detect_leaks=0 ./fuzzMe_libpng fuzzed_corpus seeds -artifact_prefix=crashes/ 2>&1 | grep -v libpng
init_wait: lptp
init_codeblock: true
init_codeblock_lang: bash
```

# PFP & libpng: LibFuzzer Output

LibFuzzer will print output that looks like this ([docs](https://llvm.org/docs/LibFuzzer.html#output)):

```
#131072 pulse  cov: 96 ft: 132 corp: 21/20Kb exec/s: 4228 rss: 636Mb
```

|    field | meaning                                            |
|---------:|----------------------------------------------------|
|    `cov` | # of basic blocks hit                              |
|    `ft`* | total signals                                      |
|   `corp` | # of entries in in-memory test corpus and its size |
| `exec/s` | executions per second                              |
|    `rss` | current memory consumption                        |

> *ft*: libFuzzer uses different signals to evaluate the code coverage: edge
> coverage, edge counters, value profiles, indirect caller/callee pairs,
> etc. These signals combined are called features (ft:).

# PFP & libpng: Goals

## Performance

Impossible to beat compiled fuzzer performance

| Method          | Exec/s | Chart                                    |
|-----------------|-------:|------------------------------------------|
| libFuzzer + pfp |  ~8k/s | ▓▓▓▓▓▓▓▓                                 |
| libFuzzer       | ~75k/s | ████████████████████████████████████████ |
  
But, perhaps we could use PFP to generate a better corpus?

# PFP & libpng: Comparing `cov` and `ft`

Since comparing raw exec/s doesn't make sense, below are comparisons of 
averaged `cov` and `ft values for pfp and libfuzzer.

| Iters - Type        | average `cov`                  | average `ft`                         |
|---------------------|--------------------------------|--------------------------------------|
| 65536 - pfp         | █████████████████▌ 96.3        | ████████████ 127.4                   |
| 65536 - libfuzzer   | █████████████████████▌ 98.8    | ██████████████████████ 136.2         |
|                     |                                |                                      |
| 131072 - pfp        | ██████████████████ 97.2        | ██████████████████ 132.7             |
| 131072 - libfuzzer  | ██████████████████████ 99.4    | ██████████████████████ 135.9         |
|                     |                                |                                      |
| 262144 - pfp        | ███████████████████▌ 97.8      | ██████████████████ 132.6             |
| 262144 - libfuzzer  | ██████████████████████ 99.5    | █████████████████████████▌ 138.3     |
|                     |                                |                                      |
| 524288 - pfp        | █████████████████████▌ 98.6    | ███████████████████▌ 133.6           |
| 524288 - libfuzzer  | ███████████████████████▌ 100.1 | ████████████████████████ 137.9       |
|                     |                                |                                      |
| 1048576 - pfp       | █████████████████████▌ 98.6    | ███████████████████▌ 133.6           |
| 1048576 - libfuzzer | ███████████████████████▌ 100.0 | ██████████████████████████ 139.0     |

# PFP & libpng: Mixing it Up

Below are the results from the previous slide, with the `cov` and `ft` results
from running half the iters with pfp, and then half with libfuzzer.

| Iters - Type        | average `cov`                  | average `ft`                         |
|---------------------|--------------------------------|--------------------------------------|
| 65536 - pfp         | █████████████████▌ 96.3        | ████████████ 127.4                   |
| 65536 - libfuzzer   | █████████████████████▌ 98.8    | ██████████████████████ 136.2         |
| 65536 - dual        | ███████████████████▌ 97.6      | █████████████████████▌ 135.0         |
|                     |                                |                                      |
| 131072 - pfp        | ██████████████████ 97.2        | ██████████████████ 132.7             |
| 131072 - libfuzzer  | ██████████████████████ 99.4    | ██████████████████████ 135.9         |
| 131072 - dual       | ██████████████████████ 99.1    | ████████████████████████ 137.3       |
|                     |                                |                                      |
| 262144 - pfp        | ███████████████████▌ 97.8      | ██████████████████ 132.6             |
| 262144 - libfuzzer  | ██████████████████████ 99.5    | █████████████████████████▌ 138.3     |
| 262144 - dual       | ██████████████████████ 99.1    | ████████████████████████ 137.8       |
|                     |                                |                                      |
| 524288 - pfp        | █████████████████████▌ 98.6    | ███████████████████▌ 133.6           |
| 524288 - libfuzzer  | ███████████████████████▌ 100.1 | ████████████████████████ 137.9       |
| 524288 - dual       | ██████████████████████ 99.5    | █████████████████████████████▌ 141.6 |
|                     |                                |                                      |
| 1048576 - pfp       | █████████████████████▌ 98.6    | ███████████████████▌ 133.6           |
| 1048576 - libfuzzer | ███████████████████████▌ 100.0 | ██████████████████████████ 139.0     |
| 1048576 - dual      | ███████████████████████▌ 100.1 | █████████████████████████████▌ 141.7 |

# Conclusion

## Benefit of Structure Aware Parsers

Structure aware parsers/mutators may 

# Conclusion

## Poor Fuzz Target

Probably would have had better performance with pfp with more complicated
targets.

# Conclusion

## Verify the PNG Template

I spent zero time here - I could have verified that *all* chunk types were
fully parsed.

Maybe some are missing?

# Conclusion

## Better PFP Mutations

I could have spent more time working on my PFP fuzzing strategy

# Conclusion

## Better PFP Loop Logic

The loop logic currently could be improved.

I think it may spend too much time recursing ontop of "successful" test cases.

# Conclusion

## Blended Results

Blended results seem to have paid off. Slightly

# Conclusion

## FIN

# Project Links

| Project                              | Link                                                     |
|--------------------------------------|----------------------------------------------------------|
| These slides + examples              | https://gitlab.com/d0c-s4vage/libfuzzer-pfp-presentation |
| pfp                                  | https://github.com/d0c-s4vage/pfp                        |
| Modified LibFuzzer (with CustomLoop) | https://gitlab.com/d0c-s4vage/libfuzzer                  |
| Modified libfuzzer-python-bridge     | https://github.com/d0c-s4vage/libfuzzer-python-bridge    |
| lookatme - what renders these slides | https://github.com/d0c-s4vage/lookatme                   |
